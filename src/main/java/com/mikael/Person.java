package com.mikael;

public abstract class Person {
    private int personalID;
    private static int id=0;
    private String firstName;
    private String lastName;
    private char gender;
    private String email;

    public Person(String firstName, String lastName, char gender, String email) {
        this.personalID = id;
        id++;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
    }
    public int getPersonalID(){
        return personalID;
    }

    public void viewAvailableDorms(Dorms dorms){
        for(Dorm dorm: dorms.availableDorms()){
            System.out.println(dorm);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "personalID=" + personalID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'';
    }
}
