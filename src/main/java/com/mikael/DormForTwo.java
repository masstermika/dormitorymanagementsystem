package com.mikael;

import java.util.LinkedList;

public class DormForTwo extends Dorm{

    public DormForTwo() {
        super("medium", 2, 300, "opened" );
    }
}
