package com.mikael;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Payment {
    private int paymentID;
    private static int id=0;
    private LocalDateTime date;
    private int amount;

    public Payment(int amount) {
        this.paymentID = id;
        id++;
        this.date = LocalDateTime.now();
        this.amount = amount;
    }

    public boolean verify(int cost){
        return this.amount>=cost;
    }


    @Override
    public String toString() {
        return "Payment{" +
                "paymentID=" + paymentID +
                ", date='" + date + '\'' +
                ", amount=" + amount +
                '}';
    }
}
