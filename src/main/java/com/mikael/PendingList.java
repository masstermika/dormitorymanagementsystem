package com.mikael;

import java.util.LinkedList;
import java.util.List;

public class PendingList {
    private final List<Student> pendingStudentsList;

    public PendingList() {
        this.pendingStudentsList = new LinkedList<>();
    }

    public List<Student> getPendingStudentsList() {
        return pendingStudentsList;
    }

    private boolean studentExists(Student student) {
        if (student != null) {
            return true;
        } else {
            System.out.println("Student does not exist!");
            return false;
        }
    }

    public boolean addStudent(Payment payment, Student student) {
        if (payment != null && studentExists(student)) {
            pendingStudentsList.add(student);
            return true;
        } else {
            System.out.println("Payment not made. Student cannot be added.");
            return false;
        }
    }



    public void removeStudent(Student student) {
        if (studentExists(student)) {

            for (Student student1 : pendingStudentsList) {
                if (student1.getPersonalID() == student.getPersonalID()) {
                    pendingStudentsList.remove(student);
                }
            }

        }
    }
}
