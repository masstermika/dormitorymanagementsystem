package com.mikael;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class Dorm {
    private final int dormID;
    private static int id=100;
    private String dormType;
    private final int capacity;
    private int cost;
    private String status;
    private  List<Student> users;

    public Dorm(String dormType, int capacity, int cost, String status) {
        this.dormID = id;
        id++;
        this.dormType = dormType;
        this.capacity=capacity;
        this.cost = cost;
        this.status = status;
        this.users=new LinkedList<>();
    }

    public void addUserList(PendingList pendingList){
        this.users.addAll(pendingList.getPendingStudentsList());
    }

    public  int cost(){
        return cost;
    }

    public int getDormID() {
        return dormID;
    }

    public String getDormType() {
        return dormType;
    }

    public void setDormType(String dormType) {
        this.dormType = dormType;
    }

    public int getCapacity() {
        return capacity;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Student> getUsers() {
        return users;
    }

    @Override
    public String toString() {
        return "Dorm{" +
                "dormID=" + dormID +
                ", dormType='" + dormType + '\'' +
                ", capacity=" + capacity +
                ", cost=" + cost +
                ", status='" + status + '\'' +
                ", users=" + users +
                '}';
    }
}
