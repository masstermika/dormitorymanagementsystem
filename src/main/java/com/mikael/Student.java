package com.mikael;

public class Student extends Person {
    private int dormID;

    public Student(String firstName, String lastName, char gender, String email) {
        super(firstName, lastName, gender, email);
        this.dormID = -1;
    }

    public Student(String firstName, String lastName, char gender, String email, int dormID) {
        super(firstName, lastName, gender, email);

        if (dormID < 0) {
            throw new IllegalArgumentException("Invalid roomID");
        }
        this.dormID = dormID;
    }

    public void setDormID(int dormID){
        this.dormID=dormID;
    }

    //TODO: viewAvaibleDorms

    public boolean hasDorm() {
        return dormID > 0;
    }


    public Payment pay(int selectedDormId, int amount, Dorms dorms,PendingList pendingList) {
        if (!hasDorm()) {
            Dorm selectedDorm = dorms.findDorm(selectedDormId);
            if (selectedDorm != null) {
                int cost = selectedDorm.cost();

                Payment payment = new Payment(amount);
                if (payment.verify(cost)) {
                    pendingList.addStudent(payment,this);
                    return payment;
                }
            }
            //TODO: aq mgoni payment verifikacia jobia movshalo da gavdaitano bookingshi imitoro gadaxdidas ragacs
            //TODO: uech ixdi da 100 magivrad 50 rogadvixado da null daabrunos aralogikuria. amitom verifikacia gaaketos bookshi
            return null;
        }
        return null;
    }
    //TODO: und davamato pendingList ramenairad. roca ixdis agdebs pending listshi




    @Override
    public String toString() {
        return "Student" +
                super.toString() +
                "dormID=" + dormID +
                '}';
    }
}
