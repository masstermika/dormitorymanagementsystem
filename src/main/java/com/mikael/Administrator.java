package com.mikael;

public class Administrator extends Person{

    public Administrator(String firstName, String lastName, char gender, String email) {
        super(firstName, lastName, gender, email);
    }

    public void viewAllDorms(Dorms dorms){
       for(Dorm dorm:dorms.allDorms()){
           System.out.println(dorm);
       }
    }

    public void bookDorm(int selectedDormId,PendingList userList,Dorms dorms){
        Dorm selectedDorm= dorms.findDorm(selectedDormId);
        if(selectedDorm!=null) {
            selectedDorm.addUserList(userList);
            for(Student student:userList.getPendingStudentsList()){
                student.setDormID(selectedDormId);
            }
        }else {
            System.out.println("Dorm doesn't exists!");
        }

    }

    @Override
    public String toString() {
        return "Administrator"+
                super.toString()+
                "}";
    }
}
