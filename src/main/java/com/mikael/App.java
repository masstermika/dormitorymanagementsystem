package com.mikael;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Dorms dorms = new Dorms();

        DormForOne dorm1 = new DormForOne();
        DormForTwo dorm2 = new DormForTwo();
        DormForThree dorm3 = new DormForThree();

        dorms.addDormForOne(dorm1);
        dorms.addDormForTwo(dorm2);
        dorms.addDormForThree(dorm3);

        Administrator administrator = new Administrator("John", "Doe", 'm', "something@gmail.com");

        Student student1 = new Student("nick", "fury", 'm', "sadasd@dsa");
        Student student2 = new Student("lily", "smith", 'f', "sadnksand@sd");
        Student student3 = new Student("bob", "marley", 'f', "sadsasnksand@sd");
        Student student4 = new Student("peter", "griffin", 'm', "sadjhyjnksand@sd");

        PendingList pendingListForDorm1 = new PendingList();

        Payment s1Payment = student1.pay(101, 300, dorms,pendingListForDorm1);
        Payment s2Payment = student2.pay(101, 302, dorms,pendingListForDorm1);


        administrator.bookDorm(101,pendingListForDorm1,dorms);

        PendingList pendingListForDorm0=new PendingList();

        Payment s3Payment = student3.pay(100, 500, dorms,pendingListForDorm0);

        administrator.bookDorm(100,pendingListForDorm0,dorms);




        System.out.println(dorms.toString());




    }
}
