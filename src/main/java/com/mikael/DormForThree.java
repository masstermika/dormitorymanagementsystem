package com.mikael;

import java.util.LinkedList;

public class DormForThree extends Dorm{
    public DormForThree() {
        super("large", 3, 200, "opened");
    }
}
