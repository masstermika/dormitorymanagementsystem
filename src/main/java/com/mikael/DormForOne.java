package com.mikael;

import java.util.LinkedList;

public class DormForOne extends Dorm{

    public DormForOne() {
        super("small", 1, 500, "opened");
    }

}
