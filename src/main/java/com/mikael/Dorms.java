package com.mikael;


import java.util.LinkedList;
import java.util.List;

public final class Dorms {
    private List<DormForOne> dormsForOne;
    private List<DormForTwo> dormsForTwo;
    private List<DormForThree> dormsForThree;

    public Dorms() {
        this.dormsForOne = new LinkedList<DormForOne>();
        this.dormsForTwo = new LinkedList<DormForTwo>();
        this.dormsForThree = new LinkedList<DormForThree>();
    }

    public void addDormForOne(DormForOne dorm){
        if(dorm!=null){
            dormsForOne.add(dorm);
        }
    }

    public void addDormForTwo(DormForTwo dorm){
        if(dorm!=null){
            dormsForTwo.add(dorm);
        }
    }

    public void addDormForThree(DormForThree dorm){
        if(dorm!=null){
            dormsForThree.add(dorm);
        }
    }



    public List<DormForOne> getDormsForOne() {
        return dormsForOne;
    }

    public List<DormForTwo> getDormsForTwo() {
        return dormsForTwo;
    }

    public List<DormForThree> getDormsForThree() {
        return dormsForThree;
    }

    public List<Dorm> availableDorms(){
        List<Dorm> result=new LinkedList<Dorm>();
        for(Dorm dorm:allDorms()){
            if(dorm.getUsers().size()>0){
                result.add(dorm);
            }
        }
        return result;
    }

    public List<Dorm> allDorms(){
        List<Dorm> result=new LinkedList<Dorm>();
        result.addAll(dormsForOne);
        result.addAll(dormsForTwo);
        result.addAll(dormsForThree);

        return result;
    }


    public Dorm findDorm(int dormID){
        for(Dorm dorm:allDorms()){
            if(dorm.getDormID()==dormID){
                return dorm;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Dorms{" +
                "dormsForOne=" + dormsForOne +
                ", dormsForTwo=" + dormsForTwo +
                ", dormsForThree=" + dormsForThree +
                '}';
    }
}
